from django.contrib import admin

from affiliate.models import Suggestion


class SuggestionAdminInline(admin.StackedInline):
    model = Suggestion
