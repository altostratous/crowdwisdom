from django.db import models

from core.models import Proposition, JudgeChoice


class Suggestion(models.Model):

    proposition = models.ForeignKey(Proposition, models.CASCADE)
    choice = models.CharField(choices=JudgeChoice.CHOICES, max_length=8)
    text = models.TextField(max_length=1024)
