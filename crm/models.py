from django.db import models


class Update(models.Model):

    title = models.CharField(max_length=1024)
    text = models.TextField(max_length=2048)
