from django.contrib import admin

from crm.models import Update


class UpdateAdmin(admin.ModelAdmin):
    list_display = ['title']


admin.site.register(Update, UpdateAdmin)

