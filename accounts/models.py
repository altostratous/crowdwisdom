# -*- coding: utf-8 -*-
from django.contrib.auth.models import User as AuthUser
from django.db import models
from django.db.models import Count, Q
from django.core.cache import cache

from crm.models import Update


class User(AuthUser):

    chat_id = models.PositiveIntegerField(null=False)
    is_sending_proposition = models.BooleanField(default=False)
    proposition_to_judge = models.ForeignKey(
        'core.Proposition', on_delete=models.SET_NULL, null=True, related_name='judging_users'
    )
    last_update_received = models.IntegerField(default=0)
    referrer = models.ForeignKey('accounts.User', on_delete=models.SET_NULL, null=True, related_name='user_set')
    subscribed = models.BooleanField(default=True, null=False)
    is_bursting = models.BooleanField(default=False, null=False)

    def save(self, *args, **kwargs):
        if not self.username:
            self.username = self.chat_id
        super().save(*args, **kwargs)

    @property
    def allowed_count(self):
        return int(
            self.judge_set.filter(id__gt=58241).count() / 20
        ) - self.proposition_set.filter(id__gt=1586).exclude(verified=False).count()

    @property
    def updates(self):
        return Update.objects.filter(id__gt=self.last_update_received).order_by('-id')[:2]

    def flush_updates(self):
        try:
            self.last_update_received = Update.objects.latest('id').id
        except Update.DoesNotExist:
            self.last_update_received = 0
        self.save()

    @property
    def badges(self):
        badges = ''

        referred_users_count = self.referred_users.count()
        if referred_users_count > 0:
            badges += self.get_badge_string(referred_users_count, '🗣')

        badges += ' '

        judge_badge_count = int(self.get_judge_count() / 50)
        if judge_badge_count > 0:
            badges += self.get_badge_string(judge_badge_count, '🎖')

        return badges or '-'

    @property
    def thor_place(self):
        pioneers = list(
            User.objects.annotate(
                referred_count=Count('user_set')
            ).order_by(
                '-referred_count'
            ).values_list(
                'id', 'referred_count'
            )
        )
        count = self.referred_users.count()
        thor_descriptor = (self.pk, count)
        last_count = None
        for i, pioneer in enumerate(pioneers):
            if pioneer == thor_descriptor:
                return i + 1, (last_count and last_count - count) or '🤷‍♀️'
            last_count = pioneer[1]
        return len(pioneers)

    @staticmethod
    def get_badge_string(count, badge):
        if count < 5:
            return badge * count
        else:
            return '{} {}'.format(count, badge)

    @property
    def referred_users(self):
        return User.objects.filter(referrer=self)

    @property
    def can_talk_to_author(self):
        return self.referred_users.count() >= 3 or self.position <= 30

    @property
    def has_telegram_username(self):
        return self.chat_id != self.username

    @property
    def is_pro(self):
        return self.get_judge_count() > 10 and self.proposition_set.count() > 1

    @property
    def is_vip(self):
        return self.position <= 30

    @property
    def position(self):
        my_score = self.score
        return len([
            score for score in self.get_cached_score_ranking()
            if score > my_score
        ]) + 1

    @property
    def score(self):
        return self.get_cached_score_dict()[self.id]

    @property
    def distance(self):
        if self.position == 1:
            return '🤷‍♀️'
        my_score = self.score
        for score in self.get_cached_score_ranking():
            if score > my_score:
                return score - my_score

    def get_cached_score_ranking(self):
        if self._score_ranking is None:
            self._score_ranking = sorted(list(self.get_cached_score_dict().values()))
        return self._score_ranking

    def get_cached_score_dict(self):

        cache_dict = cache.get('score_dict')
        if cache_dict is None:
            cache_dict = {
                record['id']: record['count'] for record in
                self.get_scores_query().values('count', 'id')
            }
            cache.set('score_dict', cache_dict)
        else:
            cache_dict[self.id] = self.judge_set.distinct().count() + self.proposition_set.distinct().filter(
                verified=True
            ).count()
            cache.set('score_dict', cache_dict)

        return cache_dict

    @staticmethod
    def get_scores_query():
        return User.objects.annotate(count=Count('judge', distinct=True) + Count('proposition_set', filter=Q(
            proposition_set__verified=True
        ), distinct=True)).order_by('count')

    def get_judge_count(self):
        return self.judge_set.count()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._score_dict = None
        self._score_ranking = None


class TalkRequest(models.Model):

    requester = models.ForeignKey(User, on_delete=models.CASCADE)
    confirmed = models.BooleanField(default=False)
    proposition = models.ForeignKey('core.Proposition', on_delete=models.CASCADE)

    class Meta:

        unique_together = ('requester', 'proposition')
