# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import Q, Count

from accounts.models import User


class Proposition(models.Model):

    text = models.CharField(max_length=1024, unique=True)
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE, related_name='proposition_set')
    verified = models.BooleanField(null=True)

    def __str__(self):
        return ' '.join(
            '{} {}'.format(
                self.judge_choice_set.filter(
                    choice=choice[0]
                ).count(),
                choice[1]
            ) for choice in JudgeChoice.CHOICES
        ) + ' ' + self.text

    @property
    def certainty(self):
        if self.judge_set.count() == 0:
            return
        return self.judge_choice_set.filter(choice=JudgeChoice.POSITIVE).count() / self.judge_set.count()

    @classmethod
    def search(cls, query):
        query_filter = Q()
        for word in query.split(' '):
            query_filter |= Q(text__contains=word)
        return cls.objects.filter(query_filter).annotate(
            judge_count=Count('judge_set')
        ).order_by(
            '-judge_count'
        )[:10]


class Judge(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    proposition = models.ForeignKey(Proposition, on_delete=models.CASCADE, related_name='judge_set')
    follow = models.BooleanField(default=False)

    class Meta:
        unique_together = ('user', 'proposition')


class JudgeChoice(models.Model):

    POSITIVE = 'p'
    NEGATIVE = 'n'
    CHOICES = (
        (POSITIVE, '👍'),
        (NEGATIVE, '👎'),
    )

    choice = models.CharField(choices=CHOICES, max_length=8)
    proposition = models.ForeignKey(Proposition, on_delete=models.CASCADE, related_name='judge_choice_set')


class RejectionChoice(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    proposition = models.ForeignKey(Proposition, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('user', 'proposition')
