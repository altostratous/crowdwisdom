from django.contrib import admin

from affiliate.admin import SuggestionAdminInline
from core.models import Proposition


def reject_propositions(modeladmin, request, queryset):
    queryset.update(verified=False)


def accept_propositions(modeladmin, request, queryset):
    queryset.update(verified=True)


reject_propositions.short_description = "Verify propositions"
reject_propositions.short_description = "Reject propositions"


class PropositionAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'verified']
    list_filter = ['verified']

    actions = [reject_propositions, accept_propositions]

    inlines = (
        SuggestionAdminInline,
    )


admin.site.register(Proposition, PropositionAdmin)
