from accounts.models import User
from crm.models import Update


def authenticate(update, referrer_user_id=None):
    user, created = authenticate_with_status(update, referrer_user_id)
    return user


def authenticate_with_status(update, referrer_user_id=None):
    chat_id = update.message.chat_id
    username = update.message.chat.username
    try:
        user = User.objects.get(chat_id=chat_id)
        if username:
            user.username = username
            user.save()
        return user, False
    except User.DoesNotExist:
        try:
            referrer = User.objects.get(id=referrer_user_id)
        except User.DoesNotExist:
            referrer = None
        return User.objects.create(
            username=username or chat_id,
            first_name=update.message.chat.first_name[:30] if update.message.chat.first_name else '',
            last_name=update.message.chat.last_name or '',
            chat_id=chat_id,
            last_update_received=Update.objects.latest('id').id,
            referrer=referrer,
        ), True
