def bot_cant_send_message_to_user(ex):
    return str(ex) == "Chat not found" or str(ex).startswith('Forbidden: bot was blocked by the user')
