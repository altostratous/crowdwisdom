import telegram
from django.db import transaction
from django.template.loader import get_template

from accounts.models import TalkRequest, User
from core.models import Proposition
from telegram_bot.utils.authentication import authenticate
from telegram_bot.utils.exceptions import bot_cant_send_message_to_user


def talk_to_author(bot, update, argument):

    user = authenticate(update)
    if not user.can_talk_to_author:
        bot.sendMessage(
            update.message.chat_id,
            text=get_template('telegram_bot/do_more.html').render(context={
                'message': 'باید اقلا ۲ کاربر را با استفاده از /share به بات آورده باشید یا رتبه‌ای بهتر یا مساوی ۳۰ '
                           'داشته باشید. '
            }),
            parse_mode=telegram.ParseMode.HTML
        )
        return
    try:
        proposition_id = int(argument)
    except ValueError:
        return
    try:
        target_proposition = Proposition.objects.get(id=proposition_id)
    except Proposition.DoesNotExist:
        return
    if not target_proposition.user.subscribed:
        return
    if not user.has_telegram_username:
        bot.sendMessage(
            update.message.chat_id,
            text=get_template('telegram_bot/username_required.html').render(),
            parse_mode=telegram.ParseMode.HTML
        )
        return
    if TalkRequest.objects.filter(requester=user, proposition=target_proposition).exists():
        return

    request_template = get_template('telegram_bot/talk_request.html')
    report_template = get_template('telegram_bot/talk_requested.html')
    target_proposition = Proposition.objects.get(id=proposition_id)
    with transaction.atomic():
        request = TalkRequest.objects.create(
            requester=user,
            proposition=target_proposition,
        )
        try:
            bot.sendMessage(
                target_proposition.user.chat_id,
                text=request_template.render(context={
                    'request': request,
                }),
                parse_mode=telegram.ParseMode.HTML
            )
        except Exception as ex:
            if bot_cant_send_message_to_user(ex):
                # hide the information
                target_proposition.user.subscribed = False
                target_proposition.user.save()
            else:
                raise ex
        bot.sendMessage(
            update.message.chat_id,
            text=report_template.render(),
            parse_mode=telegram.ParseMode.HTML
        )


def share_contact(bot, update, argument):

    user = authenticate(update)
    try:
        request_id = int(argument)
    except ValueError:
        return
    try:
        request = TalkRequest.objects.get(id=request_id, confirmed=False)
    except TalkRequest.DoesNotExist:
        return
    if not user.has_telegram_username:
        bot.sendMessage(
            update.message.chat_id,
            text=get_template('telegram_bot/username_required.html').render(),
            parse_mode=telegram.ParseMode.HTML
        )
        return

    template = get_template('telegram_bot/username_exchanged.html')
    message = template.render(context={
        'request': request,
        'user': user
    })

    with transaction.atomic():
        request.confirmed = True
        request.save()
        try:
            bot.sendMessage(
                request.requester.chat_id,
                text=message,
                parse_mode=telegram.ParseMode.HTML
            )
        except Exception as ex:
            if bot_cant_send_message_to_user(ex):
                # hide the information
                request.requester.subscribed = False
                request.requester.save()
            else:
                raise ex
        bot.sendMessage(
            update.message.chat_id,
            text=message,
            parse_mode=telegram.ParseMode.HTML
        )
