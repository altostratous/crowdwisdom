import random


def choose_proposition(propositions_to_be_judged):
    non_verified_propositions = propositions_to_be_judged.filter(verified=None)
    if non_verified_propositions.exists():
        return non_verified_propositions.order_by('id').first()
    distribution = list(propositions_to_be_judged.values_list('judge_count', flat=True))
    maximum = max(distribution)
    if [count for count in distribution if count < 30]:
        distribution = [count if count < 30 else maximum + 1 for count in distribution]
    distribution = [1 + maximum - p for p in distribution]
    random_index = random.randrange(0, sum(distribution))
    selected_index = 0
    while random_index > 0 and distribution[selected_index] > 0:
        distribution[selected_index] -= 1
        random_index -= 1
        if distribution[selected_index] == 0:
            selected_index += 1
    proposition = propositions_to_be_judged[selected_index]
    return proposition