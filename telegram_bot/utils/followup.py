import telegram
from django.template.loader import get_template

from core.models import Judge, Proposition
from telegram_bot.utils.authentication import authenticate


def unfollow(bot, update, argument):
    user = authenticate(update)
    try:
        proposition_id = int(argument)
    except ValueError:
        return
    Judge.objects.filter(user=user, proposition_id=proposition_id).update(follow=False)
    template = get_template('telegram_bot/unfollowed.html')
    bot.sendMessage(
        update.message.chat_id,
        text=template.render(),
        parse_mode=telegram.ParseMode.HTML
    )


def follow(bot, update, argument):
    user = authenticate(update)
    try:
        proposition_id = int(argument)
    except ValueError:
        return
    Judge.objects.filter(user=user, proposition_id=proposition_id).update(follow=True)
    template = get_template('telegram_bot/followed.html')
    bot.sendMessage(
        update.message.chat_id,
        text=template.render(),
        parse_mode=telegram.ParseMode.HTML
    )


def share_proposition(bot, update, argument):
    user = authenticate(update)
    try:
        proposition_id = int(argument)
    except ValueError:
        return
    template = get_template('telegram_bot/share_proposition.html')
    try:
        bot.sendMessage(
            update.message.chat_id,
            text=template.render(context={
                'proposition': Proposition.objects.get(id=proposition_id),
                'user': user
            }),
            parse_mode=telegram.ParseMode.HTML
        )
    except Proposition.DoesNotExist:
        return
