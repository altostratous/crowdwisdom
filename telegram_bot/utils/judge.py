# -*- coding: utf-8 -*-

import telegram
from django.conf import settings
from django.db import transaction
from django.db.models import Count
from django.template.loader import get_template
from telegram import ReplyKeyboardMarkup

from accounts.models import User
from core.models import Proposition, JudgeChoice, RejectionChoice, Judge
from telegram_bot.utils.authentication import authenticate
from telegram_bot.utils.mechanisms import choose_proposition

NORMAL = 'normal'
BURST = 'burst'
SHARED = 'shared'
JUDGE_MODES = (
    NORMAL, BURST, SHARED
)


def judge_given_propositions_to_be_judged(bot, update, user, mode, propositions_to_be_judged, last_proposition):
    if not propositions_to_be_judged.exists():
        if mode == SHARED:
            bot.sendMessage(
                update.message.chat_id,
                text=get_template(
                    'telegram_bot/invalid_shared_proposition.html'
                ).render({
                    'user': user,
                    'mode': mode
                }),
                parse_mode=telegram.ParseMode.HTML
            )
        else:
            bot.sendMessage(
                update.message.chat_id,
                text=get_template(
                    'telegram_bot/no_proposition.html'
                ).render(),
                parse_mode=telegram.ParseMode.HTML
            )
        return
    if user.is_bursting and mode == NORMAL and user.proposition_to_judge:
        proposition = user.proposition_to_judge
    else:
        proposition = choose_proposition(propositions_to_be_judged)
    keyboard_choices = get_user_proposition_choices(user, proposition)
    bot.sendMessage(
        update.message.chat_id,
        text=get_template(
            'telegram_bot/burst_judge.html' if mode == BURST else 'telegram_bot/judge.html'
        ).render(context={'proposition': proposition, 'last_proposition': last_proposition, 'user': user}),
        reply_markup=ReplyKeyboardMarkup(
            [keyboard_choices], resize_keyboard=True,
        )
    )
    user.proposition_to_judge = proposition
    if mode == BURST:
        user.is_bursting = True
    else:
        user.is_bursting = False
    user.save()


def judge(bot, update, mode=NORMAL, last_proposition=None):
    user = authenticate(update)
    user.is_sending_proposition = False
    user.save()
    add_first_proposition_if_none_exists()
    propositions_to_be_judged = get_users_propositions_to_be_judged(user)
    judge_given_propositions_to_be_judged(
        bot=bot,
        update=update,
        user=user,
        mode=mode,
        propositions_to_be_judged=propositions_to_be_judged,
        last_proposition=last_proposition
    )


def judge_shared_proposition(bot, update, shared_proposition_id):
    user = authenticate(update)
    user.is_sending_proposition = False
    user.save()
    add_first_proposition_if_none_exists()
    propositions_to_be_judged = get_users_propositions_to_be_judged(user)
    propositions_to_be_judged = propositions_to_be_judged.filter(id=shared_proposition_id)
    judge_given_propositions_to_be_judged(
        bot=bot,
        update=update,
        user=user,
        mode=SHARED,
        propositions_to_be_judged=propositions_to_be_judged,
        last_proposition=None
    )


def get_users_propositions_to_be_judged(user):
    propositions_to_be_judged = Proposition.objects.annotate(
        judge_count=Count('judge_set')
    ).order_by(
        'judge_count'
    ).exclude(id__in=list(
        Proposition.objects.filter(user=user).values_list('id', flat=True)
    ) + list(
        user.judge_set.values_list('proposition_id', flat=True)
    ))
    if not user.is_vip:
        propositions_to_be_judged = propositions_to_be_judged.filter(verified=True)
    else:
        propositions_to_be_judged = propositions_to_be_judged.exclude(verified=False)
    return propositions_to_be_judged


def add_first_proposition_if_none_exists():
    if not Proposition.objects.exists():
        Proposition.objects.create(text='بات عقل کل بات خوبی است.', user=User.objects.filter(
            username=settings.ADMIN_USERNAME
        ).first(), verified=True)


def get_user_proposition_choices(user: User, proposition: Proposition):
    keyboard_choices = [choice[1] for choice in JudgeChoice.CHOICES]
    if user.is_vip and proposition.verified is None:
        keyboard_choices += ['🚫']
    return keyboard_choices


def burst_judge(bot, update, last_proposition=None):
    judge(bot, update, mode=BURST, last_proposition=last_proposition)


def inform_user_judge_count(bot, proposition):
    user = User.objects.get(id=proposition.user_id)
    template = get_template('telegram_bot/proposition_getting_mature.html')
    text = template.render(context={
        'proposition': proposition,
        'user': user,
    })
    bot.sendMessage(
        user.chat_id,
        text=text,
        parse_mode=telegram.ParseMode.HTML,
        reply_markup=ReplyKeyboardMarkup(
            [['/judge']], resize_keyboard=True,
        ),
    )


def record_user_judge_from_choice_text(text, user):
    if text not in get_user_proposition_choices(user, user.proposition_to_judge):
        raise ValueError('Invalid choice {} for user {}'.format(text, user))
    with transaction.atomic():
        proposition_to_judge = user.proposition_to_judge
        Judge.objects.create(
            user=user, proposition=user.proposition_to_judge
        )
        if text == '🚫':
            RejectionChoice.objects.create(user=user, proposition=proposition_to_judge)
            if RejectionChoice.objects.filter(proposition=proposition_to_judge).count() > 1:
                proposition_to_judge.verified = False
                proposition_to_judge.save()
        else:
            choice_slug = choice_verbose_to_slug(text)
            JudgeChoice.objects.create(choice=choice_slug, proposition=proposition_to_judge)
            judge_count = JudgeChoice.objects.filter(proposition=proposition_to_judge).count()
            if judge_count > 1:
                proposition_to_judge.verified = True
                proposition_to_judge.save()
            user.proposition_to_judge = None
            user.save()


def choice_verbose_to_slug(text):
    for choice in JudgeChoice.CHOICES:
        if choice[1] == text:
            return choice[0]
