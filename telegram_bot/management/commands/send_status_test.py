from django.conf import settings

from .send_status import Command as SendStatusCommand


class Command(SendStatusCommand):
    help = 'Send status to test user'

    test_filter = {'username': settings.ADMIN_USERNAME}
