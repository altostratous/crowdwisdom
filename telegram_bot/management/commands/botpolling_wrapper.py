# -*- coding: utf-8 -*-
import random
from _signal import SIGABRT, SIGTERM, SIGINT, signal
from datetime import datetime
from time import sleep

import telegram
from django.conf import settings
from django.template.loader import get_template
from django_telegrambot.management.commands.botpolling import Command as BaseCommand

from accounts.models import User
from telegram_bot.models import GlobalParameters


class Command(BaseCommand):

    DAILY_MESSAGES = [
        'اینجا آدما از قضاوتت ناراحت نمی‌شن چون تو رو به قضاوت جمله‌شون دعوت کرده‌اند 👩‍⚖️',
        '☕️ با یک جاج عصرونه چطوری؟',
        'شاید مهارت تصمیم گیری با قضاوت جمله‌ی دیگران زیاد بشه 🤔',
        'همه چیز را همه‌گان دانند، از عقل کل آدما استفاده کن 👨‍👩‍👧‍👧',
        'قضاوت کن قبل از این که قضاوتت کنند 👩‍⚖️',
    ]

    def __init__(self, stdout=None, stderr=None, no_color=False, force_color=False):
        super().__init__(stdout, stderr, no_color, force_color)
        self.bot = None

    def get_updater(self, username=None, token=None):
        updater = super().get_updater(username, token)

        outer_self = self

        def idle_wrapper(self, stop_signals=(SIGINT, SIGTERM, SIGABRT)):
            for sig in stop_signals:
                signal(sig, self.signal_handler)

            self.is_idle = True

            while self.is_idle:
                sleep(1)
                outer_self.check_second()

        func_type = type(updater.idle)

        updater.idle = func_type(idle_wrapper, updater)

        self.bot = updater.bot

        return updater

    def check_second(self):
        parameters = GlobalParameters.get_solo()
        last_daily_message_sent = parameters.last_daily_message_sent
        if last_daily_message_sent.date() >= datetime.now().date():
            return

        if datetime.now().time().hour >= 14:
            parameters.last_daily_message_sent = datetime.now()
            parameters.save()
            self.send_daily_message()

    def send_daily_message(self):
        if settings.FEATURES['SEND_PREDEFINED_DAILY_MESSAGES']:
            self.send_predefined_daily_message()

    def send_predefined_daily_message(self):
        bot = self.bot
        users = User.objects.filter(subscribed=True)
        daily_message = self.DAILY_MESSAGES[datetime.now().date().day % len(self.DAILY_MESSAGES)]
        for user in users:
            try:
                if random.random() < 0.2:
                    bot.sendMessage(
                        chat_id=user.chat_id,
                        text=get_template('telegram_bot/daily.html').render(
                            context={'message': daily_message}
                        ),
                        parse_mode=telegram.ParseMode.HTML
                    )
            except:
                user.subscribed = False
                user.save()
                print(user.username)

