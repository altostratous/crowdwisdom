import telegram
from django.conf import settings
from django.core.management.base import BaseCommand
from django.template.loader import get_template

from accounts.models import User
from core.models import Proposition, Judge
from telegram_bot.utils.exceptions import bot_cant_send_message_to_user


class Command(BaseCommand):
    help = 'Send status to users'

    test_filter = {}

    def handle(self, *args, **options):
        bot = telegram.Bot(token=settings.DJANGO_TELEGRAMBOT['BOTS'][0]['TOKEN'])
        users = User.objects.filter(**self.test_filter)
        propositions = Proposition.objects.all()
        judges = Judge.objects.all()
        for user in users:
            if not user.subscribed:
                continue
            try:
                bot.sendMessage(
                    chat_id=user.chat_id,
                    text=get_template('telegram_bot/status.html').render(context={
                        'users': users,
                        'propositions': propositions,
                        'judges': judges,
                    }),
                    parse_mode=telegram.ParseMode.HTML
                )
                print(user.username)
            except Exception as ex:
                # user might have been gone
                if bot_cant_send_message_to_user(ex):
                    user.subscribed = False
                    user.save()
                else:
                    print(ex, user.username)
