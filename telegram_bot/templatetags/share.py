from django import template

register = template.Library()


@register.filter(is_safe=True)
def share_link(proposition):
    if not proposition:
        return ''
    if proposition.verified:
        return '\n🗣 /share_{}'.format(proposition.id)
    else:
        return ''
