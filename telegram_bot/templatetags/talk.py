from django import template

from accounts.models import TalkRequest
from core.models import Judge

register = template.Library()


@register.filter(is_safe=True)
def talk_link(proposition, user):

    if not proposition:
        return ''

    if proposition.user.id == user.id:
        return ''

    if TalkRequest.objects.filter(proposition=proposition, requester=user).exists():
        return ''

    if not proposition.user.subscribed:
        return ''

    return '\n👥 /talk_{} درخواست نام کاربری'.format(proposition.id)
