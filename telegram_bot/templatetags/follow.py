from django import template

from core.models import Judge

register = template.Library()


@register.filter(is_safe=True)
def follow_link(proposition, user):

    if not proposition:
        return ''

    if proposition.user.id == user.id:
        return ''

    if Judge.objects.get(proposition=proposition, user=user).follow:
        return '\n😚 /unfollow_{}'.format(proposition.id)
    else:
        return '\n👀 /follow_{}'.format(proposition.id)
