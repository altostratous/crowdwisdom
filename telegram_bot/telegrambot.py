# -*- coding: utf-8 -*-
from traceback import print_exc

import telegram
from django.conf import settings
from django.db.models import Q, Count
from django.template.loader import get_template
from telegram import ReplyKeyboardMarkup
from telegram.ext import CommandHandler, MessageHandler, Filters
from django_telegrambot.apps import DjangoTelegramBot

import logging

from accounts.models import User, TalkRequest
from affiliate.models import Suggestion
from core.models import Judge, Proposition
from telegram_bot.utils.authentication import authenticate, authenticate_with_status
from telegram_bot.utils.exceptions import bot_cant_send_message_to_user
from telegram_bot.utils.followup import follow, unfollow, share_proposition
from telegram_bot.utils.talk import talk_to_author, share_contact
from telegram_bot.utils.judge import judge, burst_judge, get_user_proposition_choices, \
    record_user_judge_from_choice_text, judge_shared_proposition, inform_user_judge_count, choice_verbose_to_slug

logger = logging.getLogger(__name__)


DISPATCHING_CONFIGURATION = {
    'follow': follow,
    'unfollow': unfollow,
    'share': share_proposition,
    'talk': talk_to_author,
    'contact': share_contact,
}


def process_generic_command(bot, update):
    command_text = update.message.text
    for command in DISPATCHING_CONFIGURATION:
        command_representation = '/' + command + "_"
        if command_text.startswith(command_representation):
            DISPATCHING_CONFIGURATION[command](bot, update, command_text.split(command_representation)[-1])
            return True


def start(bot, update):
    argument = update.message.text.split('/start ')[-1]
    referrer_user_id, referrer_proposition_id = None, None
    try:
        tokens = argument.split('-')
        if len(tokens) == 1:
            referrer_user_id = int(argument)
        else:
            referrer_user_id, referrer_proposition_id = map(int, tokens)
    except ValueError:
        referrer_user_id = None
    user, created = authenticate_with_status(update, referrer_user_id)
    if created:
        show_help(bot, update)
    if referrer_proposition_id:
        judge_shared_proposition(bot, update, referrer_proposition_id)
    elif not created:
        show_help(bot, update)


def consult(bot, update):
    user = authenticate(update)
    if user.allowed_count <= 0:
        bot.sendMessage(
            update.message.chat_id,
            text='شرمنده، قبل از این که به اندازه کافی قضاوت کنی نمی‌تونی جمله‌ای رو به قضاوت بذاری.',
            reply_markup=ReplyKeyboardMarkup(
                [['/judge']], resize_keyboard=True,
            ),
        )
        return

    bot.sendMessage(
        update.message.chat_id,
        text='جمله‌ای که می‌خوای قضاوت بشه رو بفرست.',
        reply_markup=ReplyKeyboardMarkup(
            [['/judge']], resize_keyboard=True,
        ),
    )

    user.is_sending_proposition = True
    user.proposition_to_judge = None
    user.save()


def show_help(bot, update):
    authenticate(update)
    template = get_template('telegram_bot/help.html')
    bot.sendMessage(
        update.message.chat_id,
        text=template.render(),
        parse_mode=telegram.ParseMode.HTML
    )


def handle_error(bot, update, error):
    error_message = 'Update "%s" caused error "%s"' % (update, error)
    print_exc()
    template = get_template('telegram_bot/error.html')
    bot.sendMessage(
        User.objects.get(username=settings.ADMIN_USERNAME).chat_id,
        text=error_message
    )
    bot.sendMessage(
        update.message.chat_id,
        text=template.render(),
        parse_mode=telegram.ParseMode.HTML
    )
    logger.error(error_message)


def process_text(bot, update):
    user = authenticate(update)
    text = update.message.text

    if user.is_sending_proposition:
        Proposition.objects.create(user=user, text=text)
        template = get_template('telegram_bot/proposition_saved.html')
        bot.sendMessage(
            update.message.chat_id,
            text=template.render(),
            parse_mode=telegram.ParseMode.HTML
        )
        user.is_sending_proposition = False
        user.save()
        return

    if user.proposition_to_judge:
        judge_choices = get_user_proposition_choices(user, user.proposition_to_judge)
        if text not in judge_choices or user.proposition_to_judge is None:
            template = get_template('telegram_bot/what.html')
            bot.sendMessage(
                update.message.chat_id,
                text=template.render(),
                parse_mode=telegram.ParseMode.HTML
            )
            return

        proposition_to_judge = user.proposition_to_judge

        record_user_judge_from_choice_text(text, user)

        proposition_judge_count = proposition_to_judge.judge_set.count()
        if proposition_judge_count >= 7 and proposition_judge_count % 2 == 1:
            try:
                inform_user_judge_count(bot, proposition_to_judge)
            except Exception as ex:
                # user might have been gone
                if bot_cant_send_message_to_user(ex):
                    proposition_to_judge.user.subscribed = False
                    proposition_to_judge.user.save()

        suggestion = None
        choice_slug = choice_verbose_to_slug(text)
        if choice_slug:
            suggestion = Suggestion.objects.filter(choice=choice_slug, proposition=proposition_to_judge).first()

        if user.is_bursting:
            burst_judge(bot, update, proposition_to_judge)
        elif not suggestion:
            template_path = 'telegram_bot/pro_thanks.html' if user.is_pro else 'telegram_bot/thanks.html'
            template = get_template(template_path)
            text = template.render(context={
                'choice': text,
                'proposition': proposition_to_judge,
                'judges': user.judge_set,
                'propositions': user.proposition_set.exclude(verified=False),
                'allowed_count': user.allowed_count,
                'updates': user.updates,
                'user': user,
            })
            bot.sendMessage(
                update.message.chat_id,
                text=text,
                parse_mode=telegram.ParseMode.HTML,
                reply_markup=ReplyKeyboardMarkup(
                    [['/judge']], resize_keyboard=True,
                ),
            )
            user.flush_updates()

        if suggestion:
            bot.sendMessage(
                update.message.chat_id,
                text=suggestion.text,
                parse_mode=telegram.ParseMode.HTML,
                reply_markup=ReplyKeyboardMarkup(
                    [['/judge']], resize_keyboard=True,
                ),
            )


def propositions(bot, update):
    user = authenticate(update)
    props = Proposition.objects.filter(Q(user=user) | Q(
        id__in=user.judge_set.filter(follow=True).values_list('proposition_id', flat=True)
    )).order_by('-id')
    text = get_template('telegram_bot/propositions.html').render(context={
        'propositions': props,
        'user': user,
    })
    for i in range(0, len(text), 4096):
        chunk = text[i: i + 4096]
        bot.sendMessage(
            update.message.chat_id,
            text=chunk,
            parse_mode=telegram.ParseMode.HTML
        )


def search(bot, update):
    user = authenticate(update)
    if user.referred_users.count() < 2:
        bot.sendMessage(
            update.message.chat_id,
            text=get_template('telegram_bot/do_more.html').render(context={
                'message': 'برای دسترسی به این امکان کافی است ۲ کاربر با دعوت شما به بات بیایند (🗣 /share).',
            }),
            parse_mode=telegram.ParseMode.HTML
        )
        return
    bot.sendMessage(
        update.message.chat_id,
        text=get_template('telegram_bot/search.html').render(context={
            'propositions': Proposition.search(query=update.message.text.split('/search ')[-1]),
        }),
        parse_mode=telegram.ParseMode.HTML
    )


def share(bot, update):
    user = authenticate(update)
    template = get_template('telegram_bot/share.html')
    bot.sendMessage(
        update.message.chat_id,
        text=template.render(context={'user': user}),
        parse_mode=telegram.ParseMode.HTML
    )


def subscribe(bot, update):
    user = authenticate(update)
    user.subscribed = True
    user.save()
    template = get_template('telegram_bot/subscribe.html')
    bot.sendMessage(
        update.message.chat_id,
        text=template.render(context={'user': user}),
        parse_mode=telegram.ParseMode.HTML
    )


def status(bot, update):
    user = authenticate(update)
    if user.username != settings.ADMIN_USERNAME:
        return
    stats = {
        'کاربران': User.objects.count(),
        'غیر آبونه': User.objects.filter(subscribed=False).count(),
        'گزاره‌ها': Proposition.objects.count(),
        'تایید‌شده': Proposition.objects.filter(verified=True).count(),
        'تعیین‌نشده': Proposition.objects.filter(verified=None).count(),
        'قضاوت‌ها': Judge.objects.count(),
        'درخواست‌‌های صحبت': TalkRequest.objects.count(),
        'درخواست‌‌های موفق صحبت': TalkRequest.objects.filter(confirmed=True).count(),
    }
    template = get_template('telegram_bot/admin_status.html')
    bot.sendMessage(
        update.message.chat_id,
        text=template.render(context={'stats': stats}),
        parse_mode=telegram.ParseMode.HTML
    )


def unsubscribe(bot, update):
    user = authenticate(update)
    user.subscribed = False
    user.save()
    template = get_template('telegram_bot/unsubscribe.html')
    bot.sendMessage(
        update.message.chat_id,
        text=template.render(context={'user': user}),
        parse_mode=telegram.ParseMode.HTML
    )


def thor(bot, update):
    user = authenticate(update)
    send_thor_status(bot, user)


def send_thor_status(bot, user):
    template = get_template('telegram_bot/thor.html')
    image_file = open('telegram_bot/static/thor.jpg', mode='rb')
    place, distance = user.thor_place
    bot.sendPhoto(
        chat_id=user.chat_id,
        caption=template.render(context={
            'place': place,
            'distance': distance,
            'count': user.referred_users.count(),
        }),
        photo=image_file)


def main():
    logger.info("Loading handlers for telegram bot")

    # Default dispatcher (this is related to the first bot in settings.DJANGO_TELEGRAMBOT['BOTS'])
    dp = DjangoTelegramBot.dispatcher
    # To get Dispatcher related to a specific bot
    # dp = DjangoTelegramBot.getDispatcher('BOT_n_token')     #get by bot token
    # dp = DjangoTelegramBot.getDispatcher('BOT_n_username')  #get by bot username

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", show_help))
    dp.add_handler(CommandHandler("judge", judge))
    dp.add_handler(CommandHandler("burst_judge", burst_judge))
    dp.add_handler(CommandHandler("consult", consult))
    dp.add_handler(CommandHandler("share", share))
    dp.add_handler(CommandHandler("subscribe", subscribe))
    dp.add_handler(CommandHandler("unsubscribe", unsubscribe))
    dp.add_handler(CommandHandler("search", search))
    dp.add_handler(CommandHandler("status", status))
    dp.add_handler(CommandHandler("thor", thor))
    dp.add_handler(CommandHandler("propositions", propositions))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.command, callback=process_generic_command))
    dp.add_handler(MessageHandler(Filters.text, callback=process_text))

    # log all errors
    dp.add_error_handler(handle_error)
