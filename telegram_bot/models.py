from datetime import datetime
from django.db import models
from solo.models import SingletonModel


class GlobalParameters(SingletonModel):

    last_daily_message_sent = models.DateTimeField(default=datetime.min)
